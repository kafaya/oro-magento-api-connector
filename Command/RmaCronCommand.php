<?php
 
namespace Smalot\MagentoBundle\Command;
 
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Sellry\MagentoRma\Entity\Rma;
use Doctrine\DBAL\DriverManager; 
use Oro\Bundle\CronBundle\Command\CronCommandInterface;
 
class RmaCronCommand extends ContainerAwareCommand implements CronCommandInterface
{
    const COMMAND_NAME   = 'oro:cron:rma-cron';
 
    /**
     * {@inheritdoc}
     */
    public function getDefaultDefinition()
    {
        return '0 * * * *';
    }
 
    /**
     * Console command configuration
     */
    public function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('OroCRM rma CRON job');
    }
 
    /**
     * Runs command
     *
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     *
     * @throws \InvalidArgumentException
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        // Retrieve default connection.
        $magento = $this->getContainer()->get('magento')->getManager();
        
        if ($magento->ping()) {
            // Call any module's class.
            $categoryManager = new \Smalot\Magento\Order\RMA($magento);
            $tree            = $categoryManager->getInfo()->execute();
        } else {
            $tree = array();
        }
            
        $magento->logout();
        foreach($tree as $single_rma) {
            //for old RMAs with 10 digit id's'
            if(strlen($single_rma['rma_id']) > 6) {
                $single_rma['rma_id'] = '#'.substr($single_rma['rma_id'], 6);    
            }
            $rma_info = $this->isRmaExist($single_rma['rma_id']);
            if(!$rma_info) {
                $rma_order_info = $this->findRmaOrder($single_rma['order_id']);
                //make sure that RMA origin order is already pulled to ORO
                if($rma_order_info) {
                    
                $print_label = unserialize($single_rma['print_label']);
                $return_addr = $print_label['streetaddress']['0'].'<br />';
                if($print_label['streetaddress']['1']) $return_addr.= $print_label['streetaddress']['1'];
                $return_addr.= $print_label['city'].', '.$print_label['stateprovince'].', '.$print_label['postcode'].'<br />'.$print_label['country_id']; 
                $conn = $this->getContainer()->get('database_connection');
                $db_array = array(
                    'id' => NULL,
                    'customer_id' => $rma_order_info['1'],
                    'order_id' => $rma_order_info['id'],
                    'store_id' => 6,
                    'user_owner_id' => 1,
                    'organization_id' => 1,
                    'channel_id' => 3,
                    'data_channel_id' => 3,
                    'increment_id' => $single_rma['order_id'],
                    'rma_id' => $single_rma['rma_id'],
                    'rma_status' => $single_rma['status'],
                    'package_opened' => $single_rma['package_opened'],
                    'request_type' => $single_rma['request_type'],
                    'return_address' => $return_addr,
                    'company' => $print_label['company'],
                    'telephone' => $print_label['telephone'],
                    'tracking_code' => $single_rma['tracking_code'],
                    'notes' => $single_rma['admin_notes'],
                    'customer_email' => $single_rma['customer_email'],
                    'currency' => 'USD',
                    'payment_method' => null,
                    'payment_details' => null,
                    'subtotal_amount' => null,
                    'shipping_amount' => null,
                    'shipping_method' => $single_rma['return_shipping'],
                    'tax_amount' => null,
                    'discount_amount' => null,
                    'discount_percent' => null,
                    'total_amount' => null,
                    'status' => $single_rma['status'],
                    'created_at' => $this->getDatetime(),
                    'updated_at' => $this->getDatetime(),
                    'first_name' => $print_label['firstname'],
                    'last_name' => $print_label['lastname']
                );
                
                $conn->insert('orocrm_magento_rma',$db_array);
                
                if(isset($single_rma['product_info'])) { 
                    
                    foreach($single_rma['product_info'] as $product) {
                        
                        $db_array = array(
                            'id' => NULL,
                            'order_id' => $rma_order_info['id'],
                            'rma_id' => $single_rma['rma_id'],
                            'owner_id' => 1,
                            'channel_id' => 3,
                            'product_type' => 'simple',
                            'product_options' => '',
                            'is_virtual' => 0,
                            'original_price' => $product['origin_price'],
                            'discount_percent' => '',
                            'name' => $product['name'],
                            'sku' => $product['sku'],
                            'qty' => $product['qty'],
                            'price' => $product['origin_price'],
                            'weight' => $product['weight'],
                            'tax_percent' => '',
                            'tax_amount' => '',
                            'discount_amount' => $product['discount_amount'],
                            'row_total' => $product['row_total'],
                            'origin_id' => $product['id']
                        );  
                        $conn->insert('orocrm_magento_rma_items',$db_array);  
                    
                    }
                
                }
                }
                else {
                    //will push rma next time (when RMA order will be pulled to ORO ))   
                }
            }
            //rma exist - update it
            else {
                if($rma_info['rmaStatus'] != $single_rma['status']) {
                    $conn = $this->getContainer()->get('database_connection'); 
                    $conn->update('orocrm_magento_rma', array('rma_status' => $single_rma['status']), array('updated_at' => $this->getDatetime()));   
                }  
            }
        }
    }
    
    public function isRmaExist($rmaId) {
    
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.rmaId, p.rmaStatus
            FROM SellryMagentoRma:Rma p
            WHERE p.rmaId = :rmaId')
            ->setParameter('rmaId', $rmaId);
        
        $rmas = $query->getResult();

        return (!empty($rmas)) ? $rmas['0'] : '';    
        
        
    }
    
    public function findRmaOrder($order_id) {
        //orocrm_magento_order
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.id, IDENTITY(p.customer)
            FROM OroCRMMagentoBundle:Order p
            WHERE p.incrementId = :order_id')
            ->setParameter('order_id', $order_id);
        
        $order_info = $query->getResult();       
        return (!empty($order_info)) ? $order_info['0'] : '';
        
    }
    
    public function getDatetime() {        
        return date('Y-m-d h:m:s');   
    }
    
    public function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }
}