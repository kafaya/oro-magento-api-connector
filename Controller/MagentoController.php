<?php

namespace Smalot\MagentoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Oro\Bundle\IntegrationBundle\Entity\Channel;
use Symfony\Component\HttpFoundation\Response;
use Sellry\MagentoRma\Entity\Rma;
use Doctrine\DBAL\DriverManager;
use Smalot\MagentoBundle\Adapter\FactoryAdapter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
/**
 * @Route("/rmasync")
 */

class MagentoController extends Controller
{
        /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var \Smalot\MagentoBundle\Logger\LoggerInterface
     */
    protected $logger;
    
    /**
     * @Route("/", name="orocrm_magento_rmasync_index")
     * @AclAncestor("orocrm_magento_order_view")
     * @Template
     */
    public function indexAction()
    {
        // Retrieve default connection.
        $magento = $this->get('magento')->getManager();
        //$tmp = new \Smalot\MagentoBundle\Adapter\FactoryAdapter();
        //$magento = $tmp->getManager();
        if ($magento->ping()) {
            // Call any module's class.
            $categoryManager = new \Smalot\Magento\Order\RMA($magento);
            $tree            = $categoryManager->getInfo()->execute();
        } else {
            $tree = array();
        }
            
        $magento->logout();
        foreach($tree as $single_rma) {
            //for old RMAs with 10 digit id's'
            if(strlen($single_rma['rma_id']) > 6) {
                $single_rma['rma_id'] = '#'.substr($single_rma['rma_id'], 6);    
            }
            $rma_info = $this->isRmaExist($single_rma['rma_id']);
            if(!$rma_info) {
                $rma_order_info = $this->findRmaOrder($single_rma['order_id']);
                //make sure that RMA origin order is already pulled to ORO
                if($rma_order_info) {
                    
                $print_label = unserialize($single_rma['print_label']);
                $return_addr = $print_label['streetaddress']['0'].'<br />';
                if($print_label['streetaddress']['1']) $return_addr.= $print_label['streetaddress']['1'];
                $return_addr.= $print_label['city'].', '.$print_label['stateprovince'].', '.$print_label['postcode'].'<br />'.$print_label['country_id']; 
                $conn = $this->get('database_connection');
                $db_array = array(
                    'id' => NULL,
                    'customer_id' => $rma_order_info['1'],
                    'order_id' => $rma_order_info['id'],
                    'store_id' => 6,
                    'user_owner_id' => 1,
                    'organization_id' => 1,
                    'channel_id' => 3,
                    'data_channel_id' => 3,
                    'increment_id' => $single_rma['order_id'],
                    'rma_id' => $single_rma['rma_id'],
                    'rma_status' => $single_rma['status'],
                    'package_opened' => $single_rma['package_opened'],
                    'request_type' => $single_rma['request_type'],
                    'return_address' => $return_addr,
                    'company' => $print_label['company'],
                    'telephone' => $print_label['telephone'],
                    'tracking_code' => $single_rma['tracking_code'],
                    'notes' => $single_rma['admin_notes'],
                    'customer_email' => $single_rma['customer_email'],
                    'currency' => 'USD',
                    'payment_method' => null,
                    'payment_details' => null,
                    'subtotal_amount' => null,
                    'shipping_amount' => null,
                    'shipping_method' => $single_rma['return_shipping'],
                    'tax_amount' => null,
                    'discount_amount' => null,
                    'discount_percent' => null,
                    'total_amount' => null,
                    'status' => $single_rma['status'],
                    'created_at' => $this->getDatetime(),
                    'updated_at' => $this->getDatetime(),
                    'first_name' => $print_label['firstname'],
                    'last_name' => $print_label['lastname']
                );
                
                $conn->insert('orocrm_magento_rma',$db_array);
                
                if(isset($single_rma['product_info'])) { 
                    
                    foreach($single_rma['product_info'] as $product) {
                        
                        $db_array = array(
                            'id' => NULL,
                            'order_id' => $rma_order_info['id'],
                            'rma_id' => $single_rma['rma_id'],
                            'owner_id' => 1,
                            'channel_id' => 3,
                            'product_type' => 'simple',
                            'product_options' => '',
                            'is_virtual' => 0,
                            'original_price' => $product['origin_price'],
                            'discount_percent' => '',
                            'name' => $product['name'],
                            'sku' => $product['sku'],
                            'qty' => $product['qty'],
                            'price' => $product['origin_price'],
                            'weight' => $product['weight'],
                            'tax_percent' => '',
                            'tax_amount' => '',
                            'discount_amount' => $product['discount_amount'],
                            'row_total' => $product['row_total'],
                            'origin_id' => $product['id']
                        );  
                        $conn->insert('orocrm_magento_rma_items',$db_array);  
                    
                    }
                
                }
                }
                else {
                    //will push rma next time (when RMA order will be pulled to ORO ))   
                }
            }
            //rma exist - update it
            else {
                $conn = $this->get('database_connection'); 
                $currentItems = $this->isRmaProductExist($single_rma['rma_id'], $conn);
                if(!$currentItems) {
                if(isset($single_rma['product_info'])) { 
                foreach($single_rma['product_info'] as $product) {
                
                    $db_array = array(
                            'id' => NULL,
                            'order_id' => $rma_order_info['id'],
                            'rma_id' => $single_rma['rma_id'],
                            'owner_id' => 1,
                            'channel_id' => 3,
                            'product_type' => 'simple',
                            'product_options' => '',
                            'is_virtual' => 0,
                            'original_price' => $product['origin_price'],
                            'discount_percent' => '',
                            'name' => $product['name'],
                            'sku' => $product['sku'],
                            'qty' => $product['qty'],
                            'price' => $product['origin_price'],
                            'weight' => $product['weight'],
                            'tax_percent' => '',
                            'tax_amount' => '',
                            'discount_amount' => $product['discount_amount'],
                            'row_total' => $product['row_total'],
                            'origin_id' => $product['id']
                        );
                          
                        $conn->insert('orocrm_magento_rma_items',$db_array);  
                       
                    }
                  }  
                    
                }      
                
                if(isset($single_rma['product_info'])) { 
                foreach($currentItems as $currentItem) {
                    if (strpos($currentItem,'Size') === false) {
                        $try = $conn->prepare("update orocrm_magento_rma_items set name = '".$single_rma['product_info'][0]['name']."' where rma_id = '".$single_rma['rma_id']."'"); 
                        $try->execute();
                    }    
                }
                }
                
                if($rma_info['rmaStatus'] != $single_rma['status']) { 
                    $try = $conn->prepare("update orocrm_magento_rma set rma_status = '".$single_rma['status']."', updated_at = '".$this->getDatetime()."' where rma_id = '".$single_rma['rma_id']."'"); 
                    $try->execute();  
                }  
            }
        }

        return new Response('<html><body><pre>' . var_export($tree, true) . '</pre></body></html>');
    }
    
    
    public function isRmaExist($rmaId) {
    
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.rmaId, p.rmaStatus
            FROM SellryMagentoRma:Rma p
            WHERE p.rmaId = :rmaId')
            ->setParameter('rmaId', $rmaId);
        
        $rmas = $query->getResult();

        return (!empty($rmas)) ? $rmas['0'] : '';    
        
        
    }
    
    public function isRmaProductExist($rmaId, $conn) {    
        return $conn->fetchAssoc('select name, rma_id from orocrm_magento_rma_items where rma_id = ?', array($rmaId));
    }
    
    public function findRmaOrder($order_id) {
        //orocrm_magento_order
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.id, IDENTITY(p.customer)
            FROM OroCRMMagentoBundle:Order p
            WHERE p.incrementId = :order_id')
            ->setParameter('order_id', $order_id);
        
        $order_info = $query->getResult();       
        return (!empty($order_info)) ? $order_info['0'] : '';
        
    }
    
    public function getDatetime() {        
        return date('Y-m-d h:m:s');   
    }
}
